/******************************************************************************
 *
 *  Copyright (C) 2003-2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/

/******************************************************************************
 *
 *  This file contains the down sampling utility to convert PCM samples in
 *  16k/32k/48k/44.1k/22050/11025 sampling rate into 8K/16bits samples
 *  required for SCO channel format. One API function isprovided and only
 *  possible to be used when transmitting SCO data is sent via HCI
 *  interface.
 *
 ******************************************************************************/
#include <string.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <inttypes.h>
#include <limits.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "bta_api.h"
#include "bta_sys.h"

#if (BTM_SCO_HCI_INCLUDED == TRUE)
#include "bt_types.h"
#include "bt_trace.h"
#include "bt_utils.h"
#include "bta_ag_api.h"
#include "device/include/esco_parameters.h"
#include "osi/include/osi.h"
#include <btm_int.h>
#include "bta_dm_co.h"
#include "osi/include/alarm.h"
#include "osi/include/thread.h"
#include <linux/ioctl.h>
#include <linux/types.h>
#include "bta_ag_int.h"
#include "hcidefs.h"
#include "bta_hf_client_int.h"

uint8_t g_sco_flow_contorl_enable_flag = 0;
pcm_file_t pcm_file_g;
uint16_t cur_timer_run_cnt = 0;
uint16_t pcm_test_start_count = 0;
uint16_t pcm_test_loop_count = 0;
uint16_t sco_controller_xmit_window;
uint16_t sco_tx_pending_cnt;
int32_t sco_socket_fd_g = INVALID_FD;
extern uint16_t sco_data_size_classic;
static uint16_t get_peer_codec_type(const RawAddress& bd_addr);
extern bt_status_t send_voice_data(RawAddress* bd_addr, uint8_t *p_buf, uint8_t len);

/*****************************************************************************
 *
 *****************************************************************************/
#ifndef BTA_DM_SCO_DEBUG
#define BTA_DM_SCO_DEBUG false
#endif
/*****************************************************************************
 *  Constants
 ****************************************************************************/

#define BTA_DM_PCM_OVERLAP_SIZE 48

#define BTA_DM_PCM_SMPL_RATE_44100 44100
#define BTA_DM_PCM_SMPL_RATE_22050 22050
#define BTA_DM_PCM_SMPL_RATE_11025 11025

/*****************************************************************************
 *  Data types for PCM Resampling utility
 ****************************************************************************/

typedef int32_t (*PCONVERT_TO_BT_FILTERED)(uint8_t* pSrc, void* pDst,
                                           uint32_t dwSrcSamples,
                                           uint32_t dwSrcSps,
                                           int32_t* pLastCurPos,
                                           uint8_t* pOverlapArea);
typedef int32_t (*PCONVERT_TO_BT_NOFILTER)(void* pSrc, void* pDst,
                                           uint32_t dwSrcSamples,
                                           uint32_t dwSrcSps);
typedef struct {
  uint8_t overlap_area[BTA_DM_PCM_OVERLAP_SIZE * 4];
  uint32_t cur_pos;                 /* current position */
  uint32_t src_sps;                 /* samples per second (source audio data) */
  PCONVERT_TO_BT_FILTERED filter;   /* the action function to do the
                             conversion 44100, 22050, 11025*/
  PCONVERT_TO_BT_NOFILTER nofilter; /* the action function to do
                               the conversion 48000, 32000, 16000*/
  uint32_t bits;                    /* number of bits per pcm sample */
  uint32_t n_channels; /* number of channels (i.e. mono(1), stereo(2)...) */
  uint32_t sample_size;
  uint32_t can_be_filtered;
  uint32_t divisor;
} tBTA_DM_PCM_RESAMPLE_CB;

tBTA_DM_PCM_RESAMPLE_CB bta_dm_pcm_cb;

/*****************************************************************************
 *  Macro Definition
 ****************************************************************************/

#define CHECK_SATURATION16(x) \
  do {                        \
    if ((x) > 32767)          \
      (x) = 32767;            \
    else if ((x) < -32768)    \
      (x) = -32768;           \
  } while (0)

////////////////////////////////////////////////////////////////////////////////
//
#define CONVERT_44100_TO_BLUETOOTH(pStart, pEnd)          \
  do {                                                    \
    int32_t out1, out2, out3, out4, out5;                 \
    SRC_TYPE* pS = (SRC_TYPE*)(pStart);                   \
    SRC_TYPE* pSEnd = (SRC_TYPE*)(pEnd);                  \
                                                          \
    while (pS < pSEnd) {                                  \
      CurrentPos -= 8000;                                 \
                                                          \
      if (CurrentPos >= 0) {                              \
        pS += SRC_CHANNELS;                               \
        continue;                                         \
      }                                                   \
      CurrentPos += dwSrcSps;                             \
                                                          \
      out1 = (SRC_SAMPLE(0) * 1587) +                     \
             ((SRC_SAMPLE(1) + SRC_SAMPLE(-1)) * 1522) +  \
             ((SRC_SAMPLE(2) + SRC_SAMPLE(-2)) * 1337) +  \
             ((SRC_SAMPLE(3) + SRC_SAMPLE(-3)) * 1058);   \
                                                          \
      out1 = out1 / 30000;                                \
                                                          \
      out2 = ((SRC_SAMPLE(4) + SRC_SAMPLE(-4)) * 725) +   \
             ((SRC_SAMPLE(5) + SRC_SAMPLE(-5)) * 384) +   \
             ((SRC_SAMPLE(6) + SRC_SAMPLE(-6)) * 79);     \
                                                          \
      out2 = out2 / 30000;                                \
                                                          \
      out3 = ((SRC_SAMPLE(7) + SRC_SAMPLE(-7)) * 156) +   \
             ((SRC_SAMPLE(8) + SRC_SAMPLE(-8)) * 298) +   \
             ((SRC_SAMPLE(9) + SRC_SAMPLE(-9)) * 345);    \
                                                          \
      out3 = out3 / 30000;                                \
                                                          \
      out4 = ((SRC_SAMPLE(10) + SRC_SAMPLE(-10)) * 306) + \
             ((SRC_SAMPLE(11) + SRC_SAMPLE(-11)) * 207) + \
             ((SRC_SAMPLE(12) + SRC_SAMPLE(-12)) * 78);   \
                                                          \
      out4 = out4 / 30000;                                \
                                                          \
      out5 = out1 + out2 - out3 - out4;                   \
                                                          \
      CHECK_SATURATION16(out5);                           \
      *psBtOut++ = (int16_t)out5;                         \
                                                          \
      pS += SRC_CHANNELS;                                 \
    }                                                     \
  } while (0)

////////////////////////////////////////////////////////////////////////////////
//
#define CONVERT_22050_TO_BLUETOOTH(pStart, pEnd)         \
  do {                                                   \
    int32_t out1, out2, out3, out4, out5;                \
    SRC_TYPE* pS = (SRC_TYPE*)(pStart);                  \
    SRC_TYPE* pSEnd = (SRC_TYPE*)(pEnd);                 \
                                                         \
    while (pS < pSEnd) {                                 \
      CurrentPos -= 8000;                                \
                                                         \
      if (CurrentPos >= 0) {                             \
        pS += SRC_CHANNELS;                              \
        continue;                                        \
      }                                                  \
      CurrentPos += dwSrcSps;                            \
                                                         \
      out1 = (SRC_SAMPLE(0) * 2993) +                    \
             ((SRC_SAMPLE(1) + SRC_SAMPLE(-1)) * 2568) + \
             ((SRC_SAMPLE(2) + SRC_SAMPLE(-2)) * 1509) + \
             ((SRC_SAMPLE(3) + SRC_SAMPLE(-3)) * 331);   \
                                                         \
      out1 = out1 / 30000;                               \
                                                         \
      out2 = ((SRC_SAMPLE(4) + SRC_SAMPLE(-4)) * 454) +  \
             ((SRC_SAMPLE(5) + SRC_SAMPLE(-5)) * 620) +  \
             ((SRC_SAMPLE(6) + SRC_SAMPLE(-6)) * 305);   \
                                                         \
      out2 = out2 / 30000;                               \
                                                         \
      out3 = ((SRC_SAMPLE(7) + SRC_SAMPLE(-7)) * 127) +  \
             ((SRC_SAMPLE(8) + SRC_SAMPLE(-8)) * 350) +  \
             ((SRC_SAMPLE(9) + SRC_SAMPLE(-9)) * 265) +  \
             ((SRC_SAMPLE(10) + SRC_SAMPLE(-10)) * 6);   \
                                                         \
      out3 = out3 / 30000;                               \
                                                         \
      out4 = ((SRC_SAMPLE(11) + SRC_SAMPLE(-11)) * 201); \
                                                         \
      out4 = out4 / 30000;                               \
                                                         \
      out5 = out1 - out2 + out3 - out4;                  \
                                                         \
      CHECK_SATURATION16(out5);                          \
      *psBtOut++ = (int16_t)out5;                        \
                                                         \
      pS += SRC_CHANNELS;                                \
    }                                                    \
  } while (0)

////////////////////////////////////////////////////////////////////////////////
//
#define CONVERT_11025_TO_BLUETOOTH(pStart, pEnd)         \
  do {                                                   \
    int32_t out1;                                        \
    SRC_TYPE* pS = (SRC_TYPE*)(pStart);                  \
    SRC_TYPE* pSEnd = (SRC_TYPE*)(pEnd);                 \
                                                         \
    while (pS < pSEnd) {                                 \
      CurrentPos -= 8000;                                \
                                                         \
      if (CurrentPos >= 0) {                             \
        pS += SRC_CHANNELS;                              \
        continue;                                        \
      }                                                  \
      CurrentPos += dwSrcSps;                            \
                                                         \
      out1 = (SRC_SAMPLE(0) * 6349) +                    \
             ((SRC_SAMPLE(1) + SRC_SAMPLE(-1)) * 2874) - \
             ((SRC_SAMPLE(2) + SRC_SAMPLE(-2)) * 1148) - \
             ((SRC_SAMPLE(3) + SRC_SAMPLE(-3)) * 287) +  \
             ((SRC_SAMPLE(4) + SRC_SAMPLE(-4)) * 675) -  \
             ((SRC_SAMPLE(5) + SRC_SAMPLE(-5)) * 258) -  \
             ((SRC_SAMPLE(6) + SRC_SAMPLE(-6)) * 206) +  \
             ((SRC_SAMPLE(7) + SRC_SAMPLE(-7)) * 266);   \
                                                         \
      out1 = out1 / 30000;                               \
                                                         \
      CHECK_SATURATION16(out1);                          \
      *psBtOut++ = (int16_t)out1;                        \
                                                         \
      pS += SRC_CHANNELS;                                \
    }                                                    \
  } while (0)

////////////////////////////////////////////////////////////////////////////////
//
#undef SRC_CHANNELS
#undef SRC_SAMPLE
#undef SRC_TYPE

#define SRC_TYPE uint8_t
#define SRC_CHANNELS 1
#define SRC_SAMPLE(x) ((pS[x] - 0x80) << 8)

/*****************************************************************************
 *  Local Function
 ****************************************************************************/
int32_t Convert_8M_ToBT_Filtered(uint8_t* pSrc, void* pDst,
                                 uint32_t dwSrcSamples, uint32_t dwSrcSps,
                                 int32_t* pLastCurPos, uint8_t* pOverlapArea) {
  int32_t CurrentPos = *pLastCurPos;
  SRC_TYPE *pIn, *pInEnd;
  SRC_TYPE *pOv, *pOvEnd;
  int16_t* psBtOut = (int16_t*)pDst;
#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG("Convert_8M_ToBT_Filtered,  CurrentPos %d\n", CurrentPos);
#endif
  memcpy(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 2), pSrc,
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  pOv = (SRC_TYPE*)(pOverlapArea + BTA_DM_PCM_OVERLAP_SIZE);
  pOvEnd = (SRC_TYPE*)(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 3));

  pIn = (SRC_TYPE*)(pSrc + BTA_DM_PCM_OVERLAP_SIZE);
  pInEnd = (SRC_TYPE*)(pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                       BTA_DM_PCM_OVERLAP_SIZE);

  if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_44100) {
    CONVERT_44100_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_44100_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_22050) {
    CONVERT_22050_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_22050_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_11025) {
    CONVERT_11025_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_11025_TO_BLUETOOTH(pIn, pInEnd);
  }

  memcpy(pOverlapArea, pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                           (BTA_DM_PCM_OVERLAP_SIZE * 2),
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  *pLastCurPos = CurrentPos;

  return (psBtOut - (int16_t*)pDst);
}

int32_t Convert_8M_ToBT_NoFilter(void* pSrc, void* pDst, uint32_t dwSrcSamples,
                                 uint32_t dwSrcSps) {
  int32_t CurrentPos;
  uint8_t* pbSrc = (uint8_t*)pSrc;
  int16_t* psDst = (int16_t*)pDst;
  int16_t sWorker;

  //      start at dwSpsSrc / 2, decrement by 8000
  //
  CurrentPos = (dwSrcSps >> 1);

  while (dwSrcSamples--) {
    CurrentPos -= 8000;

    if (CurrentPos >= 0)
      pbSrc++;
    else {
      sWorker = *pbSrc++;
      sWorker -= 0x80;
      sWorker <<= 8;

      *psDst++ = sWorker;

      CurrentPos += dwSrcSps;
    }
  }

  return (psDst - (int16_t*)pDst);
}

////////////////////////////////////////////////////////////////////////////////
//
#undef SRC_CHANNELS
#undef SRC_SAMPLE
#undef SRC_TYPE

#define SRC_TYPE int16_t
#define SRC_CHANNELS 1
#define SRC_SAMPLE(x) pS[x]

int32_t Convert_16M_ToBT_Filtered(uint8_t* pSrc, void* pDst,
                                  uint32_t dwSrcSamples, uint32_t dwSrcSps,
                                  int32_t* pLastCurPos, uint8_t* pOverlapArea) {
  int32_t CurrentPos = *pLastCurPos;
  SRC_TYPE *pIn, *pInEnd;
  SRC_TYPE *pOv, *pOvEnd;
  int16_t* psBtOut = (int16_t*)pDst;

  memcpy(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 2), pSrc,
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  pOv = (SRC_TYPE*)(pOverlapArea + BTA_DM_PCM_OVERLAP_SIZE);
  pOvEnd = (SRC_TYPE*)(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 3));

  pIn = (SRC_TYPE*)(pSrc + BTA_DM_PCM_OVERLAP_SIZE);
  pInEnd = (SRC_TYPE*)(pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                       BTA_DM_PCM_OVERLAP_SIZE);

  if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_44100) {
    CONVERT_44100_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_44100_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_22050) {
    CONVERT_22050_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_22050_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_11025) {
    CONVERT_11025_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_11025_TO_BLUETOOTH(pIn, pInEnd);
  }

  memcpy(pOverlapArea, pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                           (BTA_DM_PCM_OVERLAP_SIZE * 2),
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  *pLastCurPos = CurrentPos;

  return (psBtOut - (int16_t*)pDst);
}

int32_t Convert_16M_ToBT_NoFilter(void* pSrc, void* pDst, uint32_t dwSrcSamples,
                                  uint32_t dwSrcSps) {
  int32_t CurrentPos;
  int16_t* psSrc = (int16_t*)pSrc;
  int16_t* psDst = (int16_t*)pDst;

  //      start at dwSpsSrc / 2, decrement by 8000
  //
  CurrentPos = (dwSrcSps >> 1);

  while (dwSrcSamples--) {
    CurrentPos -= 8000;

    if (CurrentPos >= 0)
      psSrc++;
    else {
      *psDst++ = *psSrc++;

      CurrentPos += dwSrcSps;
    }
  }

  return (psDst - (int16_t*)pDst);
}

////////////////////////////////////////////////////////////////////////////////
//
#undef SRC_CHANNELS
#undef SRC_SAMPLE
#undef SRC_TYPE

#define SRC_TYPE uint8_t
#define SRC_CHANNELS 2
#define SRC_SAMPLE(x) \
  ((((pS[x * 2] - 0x80) << 8) + ((pS[(x * 2) + 1] - 0x80) << 8)) >> 1)

int32_t Convert_8S_ToBT_Filtered(uint8_t* pSrc, void* pDst,
                                 uint32_t dwSrcSamples, uint32_t dwSrcSps,
                                 int32_t* pLastCurPos, uint8_t* pOverlapArea) {
  int32_t CurrentPos = *pLastCurPos;
  SRC_TYPE *pIn, *pInEnd;
  SRC_TYPE *pOv, *pOvEnd;
  int16_t* psBtOut = (int16_t*)pDst;

#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG(
      "Convert_8S_ToBT_Filtered CurrentPos %d, SRC_TYPE %d, SRC_CHANNELS %d, \
        dwSrcSamples %d,  dwSrcSps %d",
      CurrentPos, sizeof(SRC_TYPE), SRC_CHANNELS, dwSrcSamples, dwSrcSps);
#endif
  memcpy(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 2), pSrc,
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  pOv = (SRC_TYPE*)(pOverlapArea + BTA_DM_PCM_OVERLAP_SIZE);
  pOvEnd = (SRC_TYPE*)(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 3));

  pIn = (SRC_TYPE*)(pSrc + BTA_DM_PCM_OVERLAP_SIZE);
  pInEnd = (SRC_TYPE*)(pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                       BTA_DM_PCM_OVERLAP_SIZE);

  if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_44100) {
    CONVERT_44100_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_44100_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_22050) {
    CONVERT_22050_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_22050_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_11025) {
    CONVERT_11025_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_11025_TO_BLUETOOTH(pIn, pInEnd);
  }

  memcpy(pOverlapArea, pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                           (BTA_DM_PCM_OVERLAP_SIZE * 2),
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  *pLastCurPos = CurrentPos;

  return (psBtOut - (int16_t*)pDst);
}

int32_t Convert_8S_ToBT_NoFilter(void* pSrc, void* pDst, uint32_t dwSrcSamples,
                                 uint32_t dwSrcSps) {
  int32_t CurrentPos;
  uint8_t* pbSrc = (uint8_t*)pSrc;
  int16_t* psDst = (int16_t*)pDst;
  int16_t sWorker, sWorker2;

  //      start at dwSpsSrc / 2, decrement by 8000
  //
  CurrentPos = (dwSrcSps >> 1);

  while (dwSrcSamples--) {
    CurrentPos -= 8000;

    if (CurrentPos >= 0)
      pbSrc += 2;
    else {
      sWorker = *(unsigned char*)pbSrc;
      sWorker -= 0x80;
      sWorker <<= 8;
      pbSrc++;

      sWorker2 = *(unsigned char*)pbSrc;
      sWorker2 -= 0x80;
      sWorker2 <<= 8;
      pbSrc++;

      sWorker += sWorker2;
      sWorker >>= 1;

      *psDst++ = sWorker;

      CurrentPos += dwSrcSps;
    }
  }

  return (psDst - (int16_t*)pDst);
}

////////////////////////////////////////////////////////////////////////////////
//
#undef SRC_CHANNELS
#undef SRC_SAMPLE
#undef SRC_TYPE

#define SRC_TYPE int16_t
#define SRC_CHANNELS 2
#define SRC_SAMPLE(x) ((pS[x * 2] + pS[(x * 2) + 1]) >> 1)

int32_t Convert_16S_ToBT_Filtered(uint8_t* pSrc, void* pDst,
                                  uint32_t dwSrcSamples, uint32_t dwSrcSps,
                                  int32_t* pLastCurPos, uint8_t* pOverlapArea) {
  int32_t CurrentPos = *pLastCurPos;
  SRC_TYPE *pIn, *pInEnd;
  SRC_TYPE *pOv, *pOvEnd;
  int16_t* psBtOut = (int16_t*)pDst;

  memcpy(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 2), pSrc,
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  pOv = (SRC_TYPE*)(pOverlapArea + BTA_DM_PCM_OVERLAP_SIZE);
  pOvEnd = (SRC_TYPE*)(pOverlapArea + (BTA_DM_PCM_OVERLAP_SIZE * 3));

  pIn = (SRC_TYPE*)(pSrc + BTA_DM_PCM_OVERLAP_SIZE);
  pInEnd = (SRC_TYPE*)(pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                       BTA_DM_PCM_OVERLAP_SIZE);

  if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_44100) {
    CONVERT_44100_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_44100_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_22050) {
    CONVERT_22050_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_22050_TO_BLUETOOTH(pIn, pInEnd);
  } else if (dwSrcSps == BTA_DM_PCM_SMPL_RATE_11025) {
    CONVERT_11025_TO_BLUETOOTH(pOv, pOvEnd);
    CONVERT_11025_TO_BLUETOOTH(pIn, pInEnd);
  }

  memcpy(pOverlapArea, pSrc + (dwSrcSamples * SRC_CHANNELS * sizeof(SRC_TYPE)) -
                           (BTA_DM_PCM_OVERLAP_SIZE * 2),
         BTA_DM_PCM_OVERLAP_SIZE * 2);

  *pLastCurPos = CurrentPos;

  return (psBtOut - (int16_t*)pDst);
}

int32_t Convert_16S_ToBT_NoFilter(void* pSrc, void* pDst, uint32_t dwSrcSamples,
                                  uint32_t dwSrcSps) {
  int32_t CurrentPos;
  int16_t* psSrc = (int16_t*)pSrc;
  int16_t* psDst = (int16_t*)pDst;
  int16_t sWorker;

  //      start at dwSpsSrc / 2, decrement by 8000
  //
  CurrentPos = (dwSrcSps >> 1);

  while (dwSrcSamples--) {
    CurrentPos -= 8000;

    if (CurrentPos >= 0)
      psSrc += 2;
    else {
      /* CR 82894, to avoid overflow, divide before add */
      sWorker = ((*psSrc) >> 1);
      psSrc++;
      sWorker += ((*psSrc) >> 1);
      psSrc++;

      *psDst++ = sWorker;

      CurrentPos += dwSrcSps;
    }
  }

  return (psDst - (int16_t*)pDst);
}

/*******************************************************************************
 *
 * Function         BTA_DmPcmInitSamples
 *
 * Description      initialize the down sample converter.
 *
 *                  src_sps: original samples per second (source audio data)
 *                            (ex. 44100, 48000)
 *                  bits: number of bits per pcm sample (16)
 *                  n_channels: number of channels (i.e. mono(1), stereo(2)...)
 *
 * Returns          none
 *
 ******************************************************************************/
void BTA_DmPcmInitSamples(uint32_t src_sps, uint32_t bits,
                          uint32_t n_channels) {
  tBTA_DM_PCM_RESAMPLE_CB* p_cb = &bta_dm_pcm_cb;

  p_cb->cur_pos = src_sps / 2;
  p_cb->src_sps = src_sps;
  p_cb->bits = bits;
  p_cb->n_channels = n_channels;
  p_cb->sample_size = 2;
  p_cb->divisor = 2;

  memset(p_cb->overlap_area, 0, sizeof(p_cb->overlap_area));

  if ((src_sps == BTA_DM_PCM_SMPL_RATE_44100) ||
      (src_sps == BTA_DM_PCM_SMPL_RATE_22050) ||
      (src_sps == BTA_DM_PCM_SMPL_RATE_11025))
    p_cb->can_be_filtered = 1;
  else
    p_cb->can_be_filtered = 0;

#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG("bta_dm_pcm_init_samples: n_channels = %d bits = %d",
                   n_channels, bits);
#endif
  if (n_channels == 1) {
    /* mono */
    if (bits == 8) {
      p_cb->filter = (PCONVERT_TO_BT_FILTERED)Convert_8M_ToBT_Filtered;
      p_cb->nofilter = (PCONVERT_TO_BT_NOFILTER)Convert_8M_ToBT_NoFilter;
      p_cb->divisor = 1;
    } else {
      p_cb->filter = (PCONVERT_TO_BT_FILTERED)Convert_16M_ToBT_Filtered;
      p_cb->nofilter = (PCONVERT_TO_BT_NOFILTER)Convert_16M_ToBT_NoFilter;
    }
  } else {
    /* stereo */
    if (bits == 8) {
      p_cb->filter = (PCONVERT_TO_BT_FILTERED)Convert_8S_ToBT_Filtered;
      p_cb->nofilter = (PCONVERT_TO_BT_NOFILTER)Convert_8S_ToBT_NoFilter;
    } else {
      p_cb->filter = (PCONVERT_TO_BT_FILTERED)Convert_16S_ToBT_Filtered;
      p_cb->nofilter = (PCONVERT_TO_BT_NOFILTER)Convert_16S_ToBT_NoFilter;
      p_cb->divisor = 4;
    }
  }

#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG("bta_pcm_init_dwn_sample: cur_pos %d, src_sps %d",
                   p_cb->cur_pos, p_cb->src_sps);
  APPL_TRACE_DEBUG(
      "bta_pcm_init_dwn_sample: bits %d, n_channels %d, sample_size %d, ",
      p_cb->bits, p_cb->n_channels, p_cb->sample_size);
  APPL_TRACE_DEBUG(
      "bta_pcm_init_dwn_sample: can_be_filtered %d, n_channels: %d, \
        divisor %d",
      p_cb->can_be_filtered, p_cb->n_channels, p_cb->divisor);
#endif
}

/*******************************************************************************
 * Function         BTA_DmPcmResample
 *
 * Description      Down sampling utility to convert higher sampling rate into
 *                  8K/16bits PCM samples.
 *
 * Parameters       p_src: pointer to the buffer where the original sampling PCM
 *                              are stored.
 *                  in_bytes:  Length of the input PCM sample buffer in byte.
 *                  p_dst:      pointer to the buffer which is to be used to
 *                              store the converted PCM samples.
 *
 *
 * Returns          int32_t: number of samples converted.
 *
 ******************************************************************************/
int32_t BTA_DmPcmResample(void* p_src, uint32_t in_bytes, void* p_dst) {
  uint32_t out_sample;

#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG("bta_pcm_resample : insamples  %d",
                   (in_bytes / bta_dm_pcm_cb.divisor));
#endif
  if (bta_dm_pcm_cb.can_be_filtered) {
    out_sample = (*bta_dm_pcm_cb.filter)(
        (uint8_t*)p_src, p_dst, (in_bytes / bta_dm_pcm_cb.divisor), bta_dm_pcm_cb.src_sps,
        (int32_t*)&bta_dm_pcm_cb.cur_pos, bta_dm_pcm_cb.overlap_area);
  } else {
    out_sample = (*bta_dm_pcm_cb.nofilter)((uint8_t*)p_src, p_dst,
                                           (in_bytes / bta_dm_pcm_cb.divisor),
                                           bta_dm_pcm_cb.src_sps);
  }

#if (BTA_DM_SCO_DEBUG == TRUE)
  APPL_TRACE_DEBUG("bta_pcm_resample : outsamples  %d", out_sample);
#endif

  return (out_sample * bta_dm_pcm_cb.sample_size);
}


/****************************************************
  *  Function
  ****************************************************/
static char* get_sco_tx_file_path(char* tx_file_path) {
  osi_property_get(BT_SCO_TX_FILE_PATH_PROPERTY,
       tx_file_path, DEFAULT_SCO_TX_FILE_PATH);

  return tx_file_path;
}

static char* get_sco_rx_file_path_prefix(char* rx_file_path_prefix) {
  osi_property_get(BT_SCO_RX_FILE_PATH_PREFIX_PROPERTY,
       rx_file_path_prefix, DEFAULT_SCO_RX_FILE_PATH_PREFIX);

  return rx_file_path_prefix;
}

static char* get_sco_test_loop(char* test_loop) {
  osi_property_get(BT_SCO_TEST_LOOP_PROPERTY,
       test_loop, DEFAULT_SCO_TEST_LOOP);

  return test_loop;
}

static uint16_t get_peer_codec_type(const RawAddress& bd_addr)
{
    if(property_get_bool("persist.service.bt.hfp.client", true) == true){
        tBTA_HF_CLIENT_CB* test_client_cb = NULL;
        test_client_cb = bta_hf_client_find_cb_by_bda(bd_addr);

        APPL_TRACE_DEBUG("%s, setup codec %d",
                __FUNCTION__, test_client_cb->negotiated_codec);

        return test_client_cb->negotiated_codec;
    } else if(property_get_bool("persist.service.bt.hfp.client", false) == false){
        tBTA_AG_SCO_CB* p_sco =  &bta_ag_cb.sco;
        tBTA_AG_SCB* p_scb = p_sco->p_curr_scb;
        return p_scb->sco_codec;
    } else
        return 0;
}

static uint32_t get_sco_pkt_size_by_codec(const RawAddress& bd_addr)
{

    int codec = 0;

    if(property_get_bool("persist.service.bt.hfp.client", true) == true){
        tBTA_HF_CLIENT_CB* test_hf_cb =  bta_hf_client_find_cb_by_bda(bd_addr);
        codec = test_hf_cb->negotiated_codec;
        BTM_TRACE_DEBUG("%s, intf_type:%d setup codec %d",
                __FUNCTION__, pcm_file_g.intf_type, test_hf_cb->negotiated_codec);
    } else if(property_get_bool("persist.service.bt.hfp.client", false) == false){
        tBTA_AG_SCO_CB* p_sco =  &bta_ag_cb.sco;
        tBTA_AG_SCB* test_ag_cb = p_sco->p_curr_scb;
        codec = test_ag_cb->sco_codec;
        BTM_TRACE_DEBUG("%s, intf_type:%d setup codec %d",
                __FUNCTION__, pcm_file_g.intf_type, test_ag_cb->sco_codec);
    }

    switch (pcm_file_g.intf_type)
    {
        case BT_UART_INTERFACE:
            if (codec == BTA_AG_CODEC_CVSD)
            {
                return TX_BYTES_PER_PKT_CVSD;
            }
            else if (codec == BTA_AG_CODEC_MSBC)
            {
                return TX_BYTES_PER_PKT_MSBC;
            }
            else
            {
                /* default codec */
                return TX_BYTES_PER_PKT_CVSD;
            }
            break;

        case BT_USB_INTERFACE:
            BTM_TRACE_DEBUG("%s, USB interface",
                    __FUNCTION__);

            if (codec == BTA_AG_CODEC_CVSD)
            {
                return TX_BYTES_PER_PKT_CVSD_USB;
            }
            else if (codec == BTA_AG_CODEC_MSBC)
            {
                return TX_BYTES_PER_PKT_MSBC_USB;
            }
            else
            {
                /* default codec */
                return TX_BYTES_PER_PKT_CVSD_USB;
            }
            break;

        default:
            APPL_TRACE_WARNING("%s: pcm_file_g.intf_type unknown %d",
                    __FUNCTION__, pcm_file_g.intf_type);
            return TX_BYTES_PER_PKT_CVSD;

            break;
    }
}


static void sco_test_timer_handle(UNUSED_ATTR void* context)
{
  int32_t read_count = 0;
  uint8_t read_buffer[READ_BUFFER_SIZE_MAX];
  uint32_t loop = 0;
  uint32_t loop_offset = 0;
  uint32_t pkt_num_per_trans = 0;
  uint32_t pkt_size = 0;
  int32_t read_bytes_per_trans = 0;
  int32_t tx_file_fd = pcm_file_g.tx_file_fd;
  ScoTestStatus sco_test_status = pcm_file_g.sco_test_status;

  /* validation check */
  if ( (INVALID_FD == tx_file_fd) ||(TEST_START != sco_test_status) )
  {
    APPL_TRACE_DEBUG("%s, error: tx_file_fd: %d, sco_test_status: %d",
                       __func__, tx_file_fd, sco_test_status);

    goto end_send_pkt;
  }
  /* uart flow control, just send out pkt to the queue,
  <btm_sco_check_send_pkts> will dequeue the num controlled by
  make sure pending pkt less then the max value*/
  if(sco_tx_pending_cnt >= SCO_TRANSMIT_ON_HOLD_MAX)
  {
        APPL_TRACE_DEBUG("%s sco_tx_pending_cnt:%d(max=%d), please wait...",
                       __func__, sco_tx_pending_cnt, SCO_TRANSMIT_ON_HOLD_MAX);
        return;
  }

  SCO_TRACE_DEBUG_RATE(cur_timer_run_cnt, 1/*PCM_LOG_RATE_LIMIT*/,
                                             "%s: %s, count = %d",
                                             SCO_TEST_TAG, __FUNCTION__, cur_timer_run_cnt);
  cur_timer_run_cnt ++;

  memset(read_buffer, 0, READ_BUFFER_SIZE_MAX);
  pkt_num_per_trans = pcm_file_g.pkt_num_per_trans;
  pkt_size = pcm_file_g.pkt_size;
  read_bytes_per_trans = pcm_file_g.pkt_num_per_trans * pkt_size;

  read_count = read(pcm_file_g.tx_file_fd, read_buffer, read_bytes_per_trans);

  if( (0 == read_count) || (-1 == read_count) )
  {
    APPL_TRACE_ERROR("%s: read_count = %d error, should be %d",
               __func__, read_count, read_bytes_per_trans);

    goto end_send_pkt;
  }
  else if(read_bytes_per_trans != read_count)
  {
    if(pcm_test_loop_count < pcm_file_g.sco_test_loop)
    {
      APPL_TRACE_DEBUG("%s: pcm_test_loop_count:%d, read_count = %d less than %d, assum reach the end, read from offset 0",
               __func__, pcm_test_loop_count, read_count, read_bytes_per_trans);

      pcm_test_loop_count++;
      lseek(pcm_file_g.tx_file_fd, 0, SEEK_SET);

      memset(read_buffer, 0xff, READ_BUFFER_SIZE_MAX);
    }
    else
    {
      APPL_TRACE_ERROR("%s: Whole test end: pcm_test_loop_count:%d, pcm_file_g.sco_test_loop = %d",
               __func__, pcm_test_loop_count, pcm_file_g.sco_test_loop, read_bytes_per_trans);

      goto end_send_pkt;
    }
  }
  else
  {
    /*normal state, do nothing*/
  }

  for(loop = 0; loop < pkt_num_per_trans; loop++)
  {
    sco_tx_pending_cnt++;

    /*sco_controller_xmit_window is part of SCO Flow control, which is used for Uart interface.
      it is redundancy for usb intf, add here to keep send pkt process go*/
    sco_controller_xmit_window ++;
    APPL_TRACE_DEBUG("%s: sending packet",__func__);
    send_voice_data(&(pcm_file_g.peer_addr), read_buffer + loop_offset, pkt_size);
    loop_offset += pkt_size;
  }

  return;

end_send_pkt:
  pcm_file_g.sco_test_status = TEST_STATUS_UNKNOWN;
  return;
}


static ScoTestStatus bta_sco_test_control_init(void)
{
  ScoTestStatus status = TEST_STATUS_UNKNOWN;
  bt_soc_type soc_type;
  char rx_save_path_prefix[PROPERTY_VALUE_MAX+10];

  APPL_TRACE_DEBUG("%s, soc_type: %d, intf_type: %d",
               __func__,  pcm_file_g.soc_type, pcm_file_g.intf_type);

  memset(&pcm_file_g, 0, sizeof(pcm_file_g));

  sco_controller_xmit_window = sco_data_size_classic/*SCO_TOTAL_XMIT_WIN*/;
  sco_tx_pending_cnt = 0;
  pcm_test_start_count ++;
  pcm_file_g.sco_ch_status = CH_STATUS_UNKNOWN;

  pcm_file_g.soc_type = get_soc_type();
  soc_type = pcm_file_g.soc_type;

  if(BT_SOC_SMD == soc_type)
  {
    pcm_file_g.intf_type = BT_SMD_INTERFACE;
  }
  else if (BT_SOC_ROME_USB == soc_type)
  {
    pcm_file_g.intf_type = BT_USB_INTERFACE;
    pcm_file_g.pkt_num_per_trans = PKT_NUM_PER_TRANSACTION;
  }
  else
  {
    pcm_file_g.intf_type = BT_UART_INTERFACE;
    pcm_file_g.pkt_num_per_trans = PKT_NUM_PER_TRANSACTION;
  }

  APPL_TRACE_DEBUG("%s, soc_type: %d, intf_type: %d",
               __func__,  pcm_file_g.soc_type, pcm_file_g.intf_type);

  /* get tx pcm file path */
  if(NULL == get_sco_tx_file_path(pcm_file_g.tx_file_path))
  {
    APPL_TRACE_ERROR("%s, set tx_file_path fail", __func__);

    goto init_fail;;
  }
  APPL_TRACE_DEBUG("%s, set tx_file_path: %s", __func__, pcm_file_g.tx_file_path);

  /* get tx file fd */
  pcm_file_g.tx_file_fd = open(pcm_file_g.tx_file_path, O_CREAT | O_RDWR, 0666);
  if (INVALID_FD == pcm_file_g.tx_file_fd)
  {
        APPL_TRACE_ERROR("%s, open tx_file_fd fail: %s",
                       __func__, strerror(errno));

        goto init_fail;;
  }

  /* get rx pcm file path */
  if(NULL == get_sco_rx_file_path_prefix(rx_save_path_prefix))
  {
        APPL_TRACE_ERROR("%s, set rx_save_path_prefix fail", __func__);

        goto init_fail;
  }

  sprintf(pcm_file_g.rx_save_path, "%s_%d.pcm",
               rx_save_path_prefix, pcm_test_start_count);
  APPL_TRACE_DEBUG("%s, set rx_save_path: %s",
               __func__, pcm_file_g.rx_save_path);

  /* get tx file fd */
  pcm_file_g.rx_save_fd = open(pcm_file_g.rx_save_path, O_CREAT | O_RDWR, 0666);
  if (INVALID_FD == pcm_file_g.rx_save_fd) {
        goto init_fail;
  }

  /* create hci socket, to communicate with kernel */
  if( (INVALID_FD == sco_socket_fd_g) &&
               (BT_USB_INTERFACE == pcm_file_g.intf_type))
  {
        sco_socket_fd_g = socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI);
        if (sco_socket_fd_g < 0)
        {
            APPL_TRACE_ERROR("%s: BTPROTO_HCI socket create error %s",
                           __FUNCTION__, strerror(errno));

            goto init_fail;
        }
  }

  /* get loop num */
  if(NULL == get_sco_test_loop(pcm_file_g.sco_test_loop_char))
  {
    APPL_TRACE_DEBUG("%s, 'persist.bluetooth.sco.test.loop' not set, force to 1", __func__);
    pcm_test_loop_count = 0;
    pcm_file_g.sco_test_loop = 1;
  }
  else
  {
    pcm_test_loop_count = 0;
    pcm_file_g.sco_test_loop = atoi(pcm_file_g.sco_test_loop_char);
    if(pcm_file_g.sco_test_loop > SCO_TEST_LOOP_MAX)
    {
      APPL_TRACE_DEBUG("%s, value char(%s), %d exceed: force test loop to Max %d ",
        __func__, pcm_file_g.sco_test_loop_char, pcm_file_g.sco_test_loop, SCO_TEST_LOOP_MAX);
      pcm_file_g.sco_test_loop = SCO_TEST_LOOP_MAX;
    }
    APPL_TRACE_DEBUG("%s, get_sco_test_loop char(%s), %d",
        __func__, pcm_file_g.sco_test_loop_char, pcm_file_g.sco_test_loop);
  }

  goto init_done;

init_fail:
  if(pcm_test_start_count > 0)
  {
    pcm_test_start_count --;
  }
  status =  TEST_CLOSE;
  APPL_TRACE_DEBUG("%s, init_fail: start_count:%d ", __func__, pcm_test_start_count);

  return status;

init_done:
  status = TEST_DONE_CONTROL_INIT;
  APPL_TRACE_DEBUG("%s, init_done: soc_type:%d, intf_type:%d, start_count:%d ",
               __func__, pcm_file_g.soc_type, pcm_file_g.intf_type, pcm_test_start_count);

  return status;
}


/**
  *update_usb_sco_data_channel:
  *
  *for usb interface, do isoc trans init in USB driver
  *mainly change altsetting form 0 to 2(CVSD)/4(MSBC), which will start the tx/rx poll in usb driver.
  */
static ScoTestStatus bta_sco_update_data_channel(bool enable)
{
    int sco_fd = sco_socket_fd_g;
    int ret = -1;
    ScoTestStatus status = TEST_STATUS_UNKNOWN;
    BluetoothIntfType intf_type = pcm_file_g.intf_type;
    bt_soc_type soc_type = pcm_file_g.soc_type;
    ScoDataChStatus  cur_setting = CH_STATUS_UNKNOWN;
    int alt_setting = 0;

    /*currently, sco test only support USB/UART interface*/
    if(BT_UART_INTERFACE == intf_type)
    {
        goto update_done;
    }
    else if (BT_USB_INTERFACE != intf_type)
    {
        APPL_TRACE_DEBUG("%s, intf_type: %d error (soc_type: %d)",
               __func__, intf_type, soc_type);

        goto update_fail;
    }

    cur_setting = enable?CH_ENABLE:CH_DISABLE;
    if(cur_setting == pcm_file_g.sco_ch_status)
    {
        ret = 0;
        APPL_TRACE_DEBUG("%s: cur_setting(%d) same as previous status(%d), skip update",
              __FUNCTION__, cur_setting, pcm_file_g.sco_ch_status);

        goto update_done;
    }

    if(enable)
    {
      if(pcm_file_g.codec_type ==  BTA_AG_CODEC_MSBC)
      {
        alt_setting = 4;
      }
      else /*pcm_file_g.codec_type ==  BTA_AG_CODEC_CVSD or others*/
      {
        alt_setting = 2;
      }

      APPL_TRACE_DEBUG("%s: with alt: %d", __FUNCTION__, alt_setting);
    }
    else
    {
        alt_setting = 0;
    }
    ret = ioctl(sco_fd, HCI_UPDATE_VOICE_CHANNEL, &alt_setting);

    if(0 == ret)
    {
        APPL_TRACE_DEBUG("%s: ioctl success", __FUNCTION__);

        goto update_done;
    }
    else
    {
        APPL_TRACE_ERROR("%s: ioctl fail ret: %d", __FUNCTION__, ret);

        goto update_fail;
    }

update_fail:
    status = TEST_STATUS_UNKNOWN;
    pcm_file_g.sco_ch_status = CH_STATUS_UNKNOWN;
    APPL_TRACE_ERROR("%s: intf_type %d update_fail(enable = %d), status(test/ch): %d/%d",
               __FUNCTION__, intf_type, enable, status, pcm_file_g.sco_ch_status);

    return status;

update_done:
    status = TEST_DONE_INTF_UPDATE;
    pcm_file_g.sco_ch_status = enable?CH_ENABLE:CH_DISABLE;
    APPL_TRACE_DEBUG("%s: intf_type %d update_done(enable = %d), status(test/ch): %d/%d",
               __FUNCTION__, intf_type, enable, status, pcm_file_g.sco_ch_status);

    return status;
}


int32_t bta_sco_test_intf_status_update(const RawAddress& peer_addr)
{
    ScoTestStatus pre_status = TEST_STATUS_UNKNOWN;

    APPL_TRACE_DEBUG("%s Entered",__func__);
    if(TEST_CLOSE != pcm_file_g.sco_test_status)
    {
        pre_status = pcm_file_g.sco_test_status;
        pcm_file_g.sco_test_status = TEST_STATUS_UNKNOWN;

        goto update_fail;
    }

    APPL_TRACE_DEBUG("%s g_sco_test_status",__func__);

    /*in control init*/
    pre_status = TEST_IN_CONTROL_INIT;
    pcm_file_g.sco_test_status = bta_sco_test_control_init();
    if(TEST_DONE_CONTROL_INIT != pcm_file_g.sco_test_status)
    {
        goto update_fail;
    }

    /*update peer_address and codec type*/
    pcm_file_g.peer_addr = peer_addr;
    APPL_TRACE_DEBUG("%s, peer_addr[%s]",
        __FUNCTION__, pcm_file_g.peer_addr.ToString().c_str());

    pcm_file_g.codec_type = get_peer_codec_type(peer_addr);
    APPL_TRACE_DEBUG("%s, peer_addr[%s] codec %d",
        __FUNCTION__, pcm_file_g.peer_addr.ToString().c_str(), pcm_file_g.codec_type);

    /*in intf update*/
    pre_status = TEST_IN_INTF_UPDATE;
    pcm_file_g.sco_test_status  = bta_sco_update_data_channel(TRUE);
    if(TEST_DONE_INTF_UPDATE != pcm_file_g.sco_test_status)
    {
        goto update_fail;
    }

    goto update_done;

update_fail:
    APPL_TRACE_ERROR("%s, update_fail: sco_test_status(before:%d, after%d) error",
               __func__, pre_status, pcm_file_g.sco_test_status);
    return -1;

update_done:
    APPL_TRACE_DEBUG("%s, update_done: sco_test_status(%d)", __func__, pcm_file_g.sco_test_status);
    return 0;
}

void bta_sco_test_save_voice_2_file(uint8_t* p_data, uint8_t len)
{
    ScoTestStatus status = pcm_file_g.sco_test_status;
    /*save when TEST_DONE_INTF_UPDATE will record more data,
       if want to save less, can change status check to TEST_START*/
    if( (TEST_DONE_INTF_UPDATE != status)
               && (TEST_START != status) )
    {
        APPL_TRACE_ERROR("%s: status not correct",__FUNCTION__);
        return;
    }

    if(INVALID_FD == pcm_file_g.rx_save_fd)
    {
        APPL_TRACE_ERROR("%s: rx_save_fd error(%d)",
               __FUNCTION__, pcm_file_g.rx_save_fd);
        return;
    }
    APPL_TRACE_ERROR("%s: writing PCM data to file len %d",__FUNCTION__, len);
    write(pcm_file_g.rx_save_fd, p_data, len);

    return;
}

/**start test, a timer will send out raw data periodically***/
void bta_sco_test_start(const RawAddress& peer_addr)
{
    int32_t pkt_num_per_trans = 0;
    uint32_t pkt_size = 0;

    if(TEST_DONE_INTF_UPDATE != pcm_file_g.sco_test_status)
    {
        APPL_TRACE_ERROR("%s: end test, sco_intf_status: %d error",
                       __FUNCTION__, pcm_file_g.sco_test_status);
        pcm_file_g.sco_test_status = TEST_CLOSE;

        return;
    }

    /*get peer address*/
    pcm_file_g.peer_addr = peer_addr;

    /*rest the pkt_size according to the codec type.*/
    pcm_file_g.pkt_size = get_sco_pkt_size_by_codec(peer_addr);

   APPL_TRACE_DEBUG("%s, peer_addr[%s]",
       __FUNCTION__, pcm_file_g.peer_addr.ToString().c_str());

    pkt_num_per_trans = pcm_file_g.pkt_num_per_trans;

    pkt_size = pcm_file_g.pkt_size;
    APPL_TRACE_DEBUG("%s, with - xmit_window = %d \n"
               "    - timer: %d ms; pkt: %d Bytes; pkt_count: %d)",
               __FUNCTION__, sco_controller_xmit_window,
               SCO_TEST_TIMER_MS, pkt_size, pkt_num_per_trans);

    /*start timer to send out raw data*/
    pcm_file_g.pcm_timer = alarm_new_periodic("pcm_timer");
    if(NULL == pcm_file_g.pcm_timer)
    {
      APPL_TRACE_ERROR("%s: error!!! unable to allocate pcm_timer alarm", __func__);
      pcm_file_g.sco_test_status = TEST_CLOSE;
      return;
    }
    alarm_set(pcm_file_g.pcm_timer, SCO_TEST_TIMER_MS, sco_test_timer_handle, NULL);

    pcm_file_g.sco_test_status = TEST_START;

    return;
}

void bta_sco_test_end(void)
{
    if(TEST_CLOSE == pcm_file_g.sco_test_status)
    {
        APPL_TRACE_DEBUG("%s: return, for the test is closed.",
               __FUNCTION__);
        return;
    }

    if(pcm_file_g.pcm_timer != NULL)
    {
        alarm_free(pcm_file_g.pcm_timer);
        pcm_file_g.pcm_timer = NULL;
    }

    if(TEST_DONE_INTF_UPDATE != bta_sco_update_data_channel(FALSE))
    {
        APPL_TRACE_ERROR("%s: fail %d, redo disable usb interface",
               __FUNCTION__, pcm_file_g.sco_test_status);
        if(TEST_DONE_INTF_UPDATE != bta_sco_update_data_channel(FALSE))
        {
            APPL_TRACE_ERROR("%s: disable usb interface fail: %d",
                   __FUNCTION__, pcm_file_g.sco_test_status);
        }
    }

    pcm_file_g.sco_test_status = TEST_CLOSE;

    APPL_TRACE_DEBUG("%s: sco_test_status: %d",
               __FUNCTION__, pcm_file_g.sco_test_status);
}

#endif
