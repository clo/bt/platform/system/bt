/******************************************************************************
 *
 *  Copyright (C) 2014 Google, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/

#define LOG_TAG "bt_stack_config"

#include "stack_config.h"

#include <base/logging.h>

#include "osi/include/future.h"
#include "osi/include/log.h"

const char* TRACE_CONFIG_ENABLED_KEY = "TraceConf";
const char* PTS_SECURE_ONLY_MODE = "PTS_SecurePairOnly";
const char* PTS_LE_CONN_UPDATED_DISABLED = "PTS_DisableConnUpdates";
const char* PTS_DISABLE_SDP_LE_PAIR = "PTS_DisableSDPOnLEPair";
const char* PTS_SMP_PAIRING_OPTIONS_KEY = "PTS_SmpOptions";
const char* PTS_SMP_FAILURE_CASE_KEY = "PTS_SmpFailureCase";
//LPM ++
const char *LPM_SLEEP_WAKEUP_CONFIG_KEY = "LPM_WAKEUP_CONFIG";
const char *LPM_PAGE_SCAN_PARAMS_KEY = "LPM_PAGE_SCAN_PARAMS";
const char *LPM_BLE_SCAN_PARAMS_KEY = "LPM_BLE_SCAN_PARAMS";
const char* LPM_LE_ADV_PARAMS_KEY = "LPM_BLE_ADV_PARAMS";
const char *LPM_BLE_CONN_PARAMS = "LPM_BLE_CONN_PARAMS";
const char* LPM_CRASHDUMP_KEY = "LPM_CRASHDUMP_MSG";
const char* CRITICAL_ERROR_TYPE= "CRITICAL_ERROR_TYPE";
const char* CRITICAL_ERROR_FREQ= "CRITICAL_ERROR_FREQ";
//LPM --
const char *HID_AUTOMATION_KEY = "HID_AUTOMATION";

static config_t* config;

// Module lifecycle functions

static future_t* init() {
// TODO(armansito): Find a better way than searching by a hardcoded path.
#if defined(OS_GENERIC)
  const char* path = "bt_stack.conf";
#else  // !defined(OS_GENERIC)
  const char* path = "/etc/bluetooth/bt_stack.conf";
#endif  // defined(OS_GENERIC)
  CHECK(path != NULL);

  LOG_INFO(LOG_TAG, "%s attempt to load stack conf from %s", __func__, path);

  config = config_new(path);
  if (!config) {
    LOG_INFO(LOG_TAG, "%s file >%s< not found", __func__, path);
    config = config_new_empty();
  }

  return future_new_immediate(FUTURE_SUCCESS);
}

static future_t* clean_up() {
  config_free(config);
  config = NULL;
  return future_new_immediate(FUTURE_SUCCESS);
}

EXPORT_SYMBOL extern const module_t stack_config_module = {
    .name = STACK_CONFIG_MODULE,
    .init = init,
    .start_up = NULL,
    .shut_down = NULL,
    .clean_up = clean_up,
    .dependencies = {NULL}};

// Interface functions
static bool get_trace_config_enabled(void) {
  return config_get_bool(config, CONFIG_DEFAULT_SECTION,
                         TRACE_CONFIG_ENABLED_KEY, false);
}

static bool get_pts_secure_only_mode(void) {
  return config_get_bool(config, CONFIG_DEFAULT_SECTION, PTS_SECURE_ONLY_MODE,
                         false);
}

static bool get_pts_conn_updates_disabled(void) {
  return config_get_bool(config, CONFIG_DEFAULT_SECTION,
                         PTS_LE_CONN_UPDATED_DISABLED, false);
}

static bool get_pts_crosskey_sdp_disable(void) {
  return config_get_bool(config, CONFIG_DEFAULT_SECTION,
                         PTS_DISABLE_SDP_LE_PAIR, false);
}

static const char* get_pts_smp_options(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION,
                           PTS_SMP_PAIRING_OPTIONS_KEY, NULL);
}

static int get_pts_smp_failure_case(void) {
  return config_get_int(config, CONFIG_DEFAULT_SECTION,
                        PTS_SMP_FAILURE_CASE_KEY, 0);
}

//LPM ++
static const char *get_lpm_sleep_wake_configuration(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_SLEEP_WAKEUP_CONFIG_KEY, NULL);
}

static const char *get_lpm_crashdump_key(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_CRASHDUMP_KEY, NULL);
}

static const char *get_critical_error_type(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, CRITICAL_ERROR_TYPE, NULL);
}

static const char *get_critical_error_freq(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, CRITICAL_ERROR_FREQ, NULL);
}

static const char *get_lpm_page_scan_configuration(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_PAGE_SCAN_PARAMS_KEY, NULL);
}
static const char *get_lpm_ble_scan_configuration(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_BLE_SCAN_PARAMS_KEY, NULL);
}
static const char *get_lpm_le_adv_params_configuration(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_LE_ADV_PARAMS_KEY, NULL);
}

static const char *get_lpm_ble_conn_params_configuration(void) {
  return config_get_string(config, CONFIG_DEFAULT_SECTION, LPM_BLE_CONN_PARAMS, NULL);
}
//LPM --

static bool get_hid_logging_configuration(void) {
  return config_get_bool(config, CONFIG_DEFAULT_SECTION, HID_AUTOMATION_KEY,false);
}

static config_t* get_all(void) { return config; }

const stack_config_t interface = {get_trace_config_enabled,
                                  get_pts_secure_only_mode,
                                  get_pts_conn_updates_disabled,
                                  get_pts_crosskey_sdp_disable,
                                  get_pts_smp_options,
                                  get_pts_smp_failure_case,
                                  get_all,
                                  //LPM ++
                                  get_lpm_sleep_wake_configuration,
                                  get_lpm_page_scan_configuration,
                                  get_lpm_ble_scan_configuration,
                                  get_lpm_le_adv_params_configuration,
                                  get_lpm_ble_conn_params_configuration,
		                  get_lpm_crashdump_key,
                                  //LPM --
				  get_critical_error_type,
                                  get_critical_error_freq,
                                  get_hid_logging_configuration
                                  };

const stack_config_t* stack_config_get_interface(void) { return &interface; }
